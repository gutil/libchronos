/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libchronos
 * FILE NAME: print_time.c
 *
 * CONTRIBUTORS: Nicolas FLIPO, Shuaitao WANG, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: Checks CPU time.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libchronos Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <stdarg.h>
#include <time.h>
#include <math.h>
#include <libprint.h>
#include <time_series.h>
#include "CHR.h"

double CHR_anal_time(double t1, double ttot) {
  double pourc;

  if (fabs(ttot) > EPS_CHR)
    pourc = t1 / ttot;
  else
    pourc = (double)CODE_TS;

  return pourc;
}

void CHR_print_times(FILE *fp, s_clock_CHR *pclock) {
  int i;
  double time_tot = 0.;
  double length, tl;
  char *name_tmp;

  LP_printf(fp, "Time spent to achieve different calculations\n\n");

  tl = pclock->time_length;

  for (i = 0; i < NOP_CHR; i++) {
    length = pclock->time_spent[i] * 100;
    name_tmp = CHR_name_op(i);
    LP_printf(fp, "%s: %f s (%6.2f %)\n", name_tmp, pclock->time_spent[i], CHR_anal_time(length, tl));
    time_tot += pclock->time_spent[i];
    free(name_tmp);
  }

  LP_printf(fp, "Other calculations: %f s (%6.2f %%)\n", pclock->time_length - time_tot, CHR_anal_time((tl - time_tot) * 100, tl));
  LP_printf(fp, "\nTime spent for the whole simulation: %f s\n", tl);
}