/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libchronos
 * FILE NAME: manage_chronos.c
 *
 * CONTRIBUTORS: Nicolas FLIPO, Shuaitao WANG, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: Checks CPU time.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libchronos Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <time_series.h>
#include <libprint.h>
#include <CHR.h>
#include <math.h>

s_chronos_CHR *CHR_init_chronos() {
  s_chronos_CHR *pchronos;
  int i;

  pchronos = CHR_new_chronos();
  bzero((char *)pchronos, sizeof(s_chronos_CHR));
  pchronos->t[BEGINNING_TS] = (double)CODE_TS;
  pchronos->t[END_TS] = (double)CODE_TS;
  pchronos->dt = DT_CHR; // NF 21/10/2017 pour verif ulterieure

  for (i = 0; i < NEXTREMA_CHR; i++) {
    pchronos->pd[i] = new_date_ts();
    bzero((char *)pchronos->pd[i], sizeof(s_date_ts));
  }
  pchronos->yr0 = INITIAL_YEAR_JULIAN_DAY_TS; // NF 3/12/2020

  return pchronos;
}

/*Set-up chronos for steady state*/
void *CHR_reinit_chronos(s_chronos_CHR *pchronos) {
  pchronos->t[BEGINNING_TS] = (double)CODE_TS;
  pchronos->t[END_TS] = (double)CODE_TS;
  pchronos->dt = DT_CHR;
}

int CHR_nj_sim(s_chronos_CHR *pchronos) {
  int nj, year;
  int nyr;

  nj = 0;

  for (nyr = 0; nyr < pchronos->nyear; nyr++) {
    year = pchronos->year[BEGINNING_CHR] + nyr;
    nj += TS_nb_jour_an(year + 1);
  }

  return nj;
}

double CHR_convert_time(double nb_time, int in_units, int out_units) {
  double out_time;
  out_time = CODE;

  if (in_units == DAYS_CHR && out_units == S_CHR) {
    out_time = nb_time * NSEC_DAY_TS; // NF 3/12/2020
  }

  if (in_units == S_CHR && out_units == DAYS_CHR)
    out_time = nb_time * CONV_SEC_DDEC_TS; // NF 3/12/2020

  return out_time;
}

void CHR_refresh_t(s_chronos_CHR *chronos) {
  chronos->t[BEGINNING_CHR] = chronos->t[CUR_CHR];
  chronos->t[CUR_CHR] += chronos->dt;
}

void CHR_refresh_year(s_chronos_CHR *chronos, int y) { chronos->year[CUR_CHR] = chronos->year[BEGINNING_CHR] + y; }

s_chronos_CHR *CHR_copy_chronos(s_chronos_CHR *p1) {
  s_chronos_CHR *pchronos;
  int i;

  pchronos = CHR_new_chronos();

  for (i = 0; i < NEXTREMA_CHR; i++) {
    pchronos->t[i] = p1->t[i];
    pchronos->year[i] = p1->year[i];
  }

  pchronos->dt = p1->dt;
  pchronos->day_d = p1->day_d;
  pchronos->hour_h = p1->hour_h;
  pchronos->month = p1->month;

  for (i = 0; i < NEXTREMA_CHR; i++) { // NF 3/12/2020
    pchronos->pd[i] = new_date_ts();
    pchronos->pd[i] = p1->pd[i];
  }

  pchronos->yr0 = p1->yr0;

  return pchronos;
}

// NF 3/12/2020 Updates all the dates structures from the t array of a chronos structur
s_chronos_CHR *CHR_update_day_from_time(s_chronos_CHR *pchronos, FILE *flog) {
  int i;
  for (i = 0; i < NEXTREMA_CHR; i++) { // NF 3/12/2020 watch maybe useless to update END_TS
    pchronos->pd[i] = TS_convert_julian2date_decimal(TS_convert_seconds2days(pchronos->t[i], flog), pchronos->yr0, flog);
  }
}

// NF 3/12/2020 Updates the itypt_time date structure from the t[itypt_time] of a chronos structur
s_chronos_CHR *CHR_update_specific_day_from_time(s_chronos_CHR *pchronos, int itype_time, FILE *flog) {
  int i = itype_time;
  pchronos->pd[i] = TS_convert_julian2date_decimal(TS_convert_seconds2days(pchronos->t[i], flog), pchronos->yr0, flog);
}