/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libchronos
 * FILE NAME: manage_clock.c
 *
 * CONTRIBUTORS: Nicolas FLIPO, Shuaitao WANG, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: Checks CPU time.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libchronos Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <libprint.h>
#include <time_series.h>
#include <CHR.h>

void CHR_init_timer(s_clock_CHR *pclock) {
  int i;

  for (i = 0; i < NOP_CHR; i++) {
    pclock->time_spent[i] = 0.;
  }
}

s_clock_CHR *CHR_init_clock() {
  s_clock_CHR *pclock;

  pclock = CHR_new_clock();
  bzero((char *)pclock, sizeof(s_clock_CHR));
  pclock->begin = time(NULL);
  CHR_init_timer(pclock);

  return pclock;
}