/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libchronos
 * FILE NAME: itos.c
 *
 * CONTRIBUTORS: Nicolas FLIPO, Shuaitao WANG, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: Checks CPU time.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libchronos Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <time.h>
#include <libprint.h>
#include <time_series.h>
#include "CHR.h"

char *CHR_name_op(int var) {
  char *name;

  switch (var) {
  case LEC_CHR: {
    name = strdup("Lecture of the input data");
    break;
  }
  case TAB_CHR: {
    name = strdup("Tabulation of objects");
    break;
  }
  case INIT_CHR: {
    name = strdup("Initialisation");
    break;
  }
  case MATRIX_FILL_CHR: {
    name = strdup("Calculation of the matrix coefficients and filling libgc for hydraulic simulation");
    break;
  }
  case MATRIX_SOLVE_CHR: {
    name = strdup("Solving of the hydraulic system with libgc");
    break;
  }
  case MATRIX_GET_CHR: {
    name = strdup("Extraction of the hydraulic state variables calculated by libgc");
    break;
  }
  case MAT_FILL_AQ: {
    name = strdup("Calculation of the matrix coefficients and filling libgc for groundwater flow simulation");
    break;
  }
  case MAT_SOLVE_AQ: {
    name = strdup("Solving of the groundwater system with libgc");
    break;
  }
  case MAT_GET_AQ: {
    name = strdup("Extraction of the groundwater state variables calculated by libgc");
    break;
  }
  case OUTPUTS_CHR: {
    name = strdup("Printing of the output files");
    break;
  }
  case NSAT_CHR: {
    name = strdup("Calculation of ZNS water fluxes");
    break;
  }
  case FP_CHR: {
    name = strdup("Calculation of surface water balance");
    break;
  }
  case SOLVE_TTC: {
    name = strdup("Calculation of transport");
    break;
  }
  case SOLVE_RIVE: {
    name = strdup("Calculation of biology");
    break;
  }
  case SEB_CHR: {
    name = strdup("Calculation of libseb");
    break;
  }
  case HDERM_CHR: {
    name = strdup("Calculation of waterflow inputs to hyperdermic river network");
    break;
  }
  case FP_TRANS_CHR: {
    name = strdup("Calculation of transport surface fluxes");
    break;
  }
  case NSAT_TRANS_CHR: {
    name = strdup("Calculation of transport in unsaturated zone");
    break;
  }
  case HDERM_TRANS_CHR: {
    name = strdup("Calculation of transport inputs to hyperdermic river network");
    break;
  }
  case AQ_HIN_CHR: {
    name = strdup("Calculation of water recharge to aquifer system");
    break;
  }
  case AQ_TRIN_CHR: {
    name = strdup("Calculation of transport inputs to aquifer system");
    break;
  }
  default: {
    name = strdup("Unknown calculation variable type in itos.c");
    break;
  }
  }

  return name;
}