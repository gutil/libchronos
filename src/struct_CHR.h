/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libchronos
 * FILE NAME: struct_CHR.h
 *
 * CONTRIBUTORS: Nicolas FLIPO, Shuaitao WANG, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: Checks CPU time.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libchronos Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <time.h>

typedef struct clock_CHR s_clock_CHR;
typedef struct chronos_CHR s_chronos_CHR;

/* Structure describing the time characteristics of the simulation */
struct chronos_CHR {
  /* Table containing the times of the beginning and of the end of the simulation */
  double t[NEXTREMA_CHR];
  /* Global time step */
  double dt;
  /* Global output time step */
  double dt_output;
  /* Day in the current month */
  int day_d;
  /* Hour in the current day (>= 0 and < 24) */
  double hour_h;
  /* Current month and year in simulation */
  int month;
  /* Current year */
  int year[NEXTREMA_CHR];
  /* simulation length in year*/
  int nyear;
  /* Table containing the dates of the beginning and of the end of the
   * simulation, as well as the CURRENT one */ //NF 28/11/2020
  s_date_ts *pd[NEXTREMA_CHR];
  /* YEAR 0 for julian days : allows the definition
   * of the O for julian days */
  int yr0;
};

/* Structure of the timer enabling to determine the time spent for different calculations in the model */
struct clock_CHR {
  /* Date of simulation */
  char *date;
  /* Times of beginning and end of the simulation */
  time_t begin, end;
  /* Simulation length */
  double time_length;
  /* Time spent for different calculations */
  double time_spent[NOP_CHR];
  /*Time spent for different processes to be defined in the library that is checked*/
  double *check_time;
};

#define CHR_new_chronos() ((s_chronos_CHR *)malloc(sizeof(s_chronos_CHR)))
#define CHR_new_clock() ((s_clock_CHR *)malloc(sizeof(s_clock_CHR)))