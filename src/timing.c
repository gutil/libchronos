/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libchronos
 * FILE NAME: timing.c
 *
 * CONTRIBUTORS: Nicolas FLIPO, Shuaitao WANG, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: Checks CPU time.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libchronos Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <stdarg.h>
#include <time.h>
#include <math.h>
#include <libprint.h>
#include <time_series.h>
#include "CHR.h"

/* Valeur initiale du temps  */
static struct timeval time_begin;

/* Valeur finale du temps  */
static struct timeval time_end;

void testerreur(char *format, int retour, ...) {
  va_list pvar;
  char buffer[1000];

  va_start(pvar, retour);

  if (retour < 0) {
    vsprintf(buffer, format, pvar);
    perror(buffer);
    exit(1);
  }
}

/* Initialise la variable temps time_begin */
void CHR_begin_timer() { testerreur("gettimeofday", gettimeofday(&time_begin, NULL)); }

/* Estime le temps écoulé depuis le dernier appel à begin_timer() */
double CHR_end_timer() {
  double run_time;

  testerreur("gettimeofday", gettimeofday(&time_end, NULL));
  /* Take care of the non-associativity in floating point :-) */
  run_time = time_end.tv_sec - time_begin.tv_sec + (time_end.tv_usec - time_begin.tv_usec) / 1e6;

  return run_time;
}

/* Stops the simulation timer and memorizes the simulation's length */
void CHR_calculate_calculation_length(s_clock_CHR *pclock, FILE *fp) {
  pclock->end = time(NULL);
  pclock->time_length = difftime(pclock->end, pclock->begin);

  if (fabs(pclock->time_length) < EPS_CHR)
    LP_warning(fp, "In libchronos%4.2f: Calculation time very short %f\n\tCheck it is correct\n", VERSION_CHR, pclock->time_length);
  else
    LP_printf(fp, "In libchronos%4.2f: Calculation time %f\n", VERSION_CHR, pclock->time_length);
}

/* Calculates the date corresponding to a <<chronos>> structure */
void CHR_calculate_simulation_date(s_chronos_CHR *chronos, double dt) {
  /* Table containing the number of days in each month */
  int ndays[12];
  int nj; // number of day for the year
  int bissex = NO_TS;

  nj = TS_nb_jour_an(chronos->year[BEGINNING_CHR]);
  bissex = TS_is_bissex(nj);

  chronos->hour_h += dt / 3600.;

  while (chronos->hour_h >= 24.) {
    chronos->hour_h -= 24.;
    chronos->day_d++;
    ndays[chronos->month] = TS_nday_month(chronos->month);

    while ((chronos->day_d > ndays[chronos->month]) || ((chronos->month == FEBRUARY) && (bissex == YES_TS) && (chronos->day_d == 29))) {
      chronos->day_d -= ndays[chronos->month];
      chronos->month++;

      if (chronos->month == 13) {
        chronos->month = JANUARY;
        chronos->year[BEGINNING_CHR]++;
      }
      ndays[chronos->month] = TS_nday_month(chronos->month);
    }
  }
}