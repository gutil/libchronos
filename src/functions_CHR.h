/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libchronos
 * FILE NAME: functions_CHR.h
 *
 * CONTRIBUTORS: Nicolas FLIPO, Shuaitao WANG, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: Checks CPU time.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libchronos Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

// in manage_clock.c
s_clock_CHR *CHR_init_clock();

// un manage_chronos.c
s_chronos_CHR *CHR_init_chronos();
int CHR_nj_sim(s_chronos_CHR *);
double CHR_convert_time(double, int, int);
void CHR_refresh_t(s_chronos_CHR *);
void CHR_refresh_year(s_chronos_CHR *, int);
s_chronos_CHR *CHR_copy_chronos(s_chronos_CHR *);
void *CHR_reinit_chronos(s_chronos_CHR *pchronos);
s_chronos_CHR *CHR_update_day_from_time(s_chronos_CHR *, FILE *);
s_chronos_CHR *CHR_update_specific_day_from_time(s_chronos_CHR *, int, FILE *);

// in print_time.c
void CHR_print_times(FILE *, s_clock_CHR *);

// in itos.c
char *CHR_name_op(int);

// in timing.c
void CHR_init_timer(s_clock_CHR *);
void CHR_begin_timer();
double CHR_end_timer();
void CHR_calculate_calculation_length(s_clock_CHR *, FILE *);
void CHR_calculate_simulation_date(s_chronos_CHR *, double);