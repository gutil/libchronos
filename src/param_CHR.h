/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libchronos
 * FILE NAME: param_CHR.h
 *
 * CONTRIBUTORS: Nicolas FLIPO, Shuaitao WANG, Nicolas GALLOIS
 *
 * LIBRARY BRIEF DESCRIPTION: Checks CPU time.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libchronos Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#define EPS_CHR EPS_TS
#define DT_CHR 86400
#define VERSION_CHR 0.13

/* NG : 31/07/2020 FP_TRANS_CHR and NSAT_TRANS_CHR added for
 * transport flux calculation times in libfp, libnsat, hderm
 * SW : 15/06/2021 SEB_CHR for libseb */
enum calc_types_chr { LEC_CHR, TAB_CHR, INIT_CHR, MATRIX_FILL_CHR, MATRIX_SOLVE_CHR, MATRIX_GET_CHR, MAT_FILL_AQ, MAT_SOLVE_AQ, MAT_GET_AQ, OUTPUTS_CHR, NSAT_CHR, FP_CHR, SOLVE_TTC, SOLVE_RIVE, SEB_CHR, HDERM_CHR, FP_TRANS_CHR, NSAT_TRANS_CHR, HDERM_TRANS_CHR, AQ_HIN_CHR, AQ_TRIN_CHR, NOP_CHR };

enum beginning_end_chr { BEGINNING_CHR, END_CHR, CUR_CHR, NEXTREMA_CHR };

enum time_units_CHR { S_CHR, MINS_CHR, HOURS_CHR, DAYS_CHR, MONTH_CHR, YEAR_CHR };